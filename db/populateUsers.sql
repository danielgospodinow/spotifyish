USE project;

/* Fake users password: 'Adm1n4e' */

INSERT INTO users(nickname, password) VALUES ("daniel_gospodinov", "$2y$10$x5nLgcL67VpZjkAZW3Pr0.Hji7KievVX4G8zDcvFhz3WvmmhH6LYC");
INSERT INTO users(nickname, password) VALUES ("alexander_dudev", "$2y$10$x5nLgcL67VpZjkAZW3Pr0.Hji7KievVX4G8zDcvFhz3WvmmhH6LYC");
INSERT INTO users(nickname, password) VALUES ("alexander_zaharyan", "$2y$10$x5nLgcL67VpZjkAZW3Pr0.Hji7KievVX4G8zDcvFhz3WvmmhH6LYC");

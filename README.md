# Spotifyish

Проект по Уеб Технологии.

Функционалности:
 - Регистрация и логин в системата
 - Слушане на музика през вграден аудио плейър
 - Харесване на песен
 - Пазене на история за изслушаните песни
 - Статистики относно песни (най-слушана песен - индивидуално и за всички потребители)
 - Качване на песен

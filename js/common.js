function markSelected(target) {
	if(target != undefined) {
		const targetId = target.attributes[0].value
		const element = document.getElementById(targetId)
		element.classList.add("active")
	}
}
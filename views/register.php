<!DOCTYPE html>
<html>
<?php include "templates/head.php";?>

<link rel="stylesheet" type="text/css" href="../styles/registerStyle.css">
<link rel="stylesheet" type="text/css" href="../styles/homeStyle.css">

<body>
    <div class="ver">
    <div class="container">
        <form id='form' action="../controllers/registerController.php" onsubmit="return validate()" method="POST">
            <img src="../static/logo.png" width="72"/>
            <h1>РЕГИСТРАЦИЯ</h1>
            <input required maxlength="10" placeholder="  Потребителско име" type="text" name="nickname">
            <input required maxlength="10" placeholder="  Парола" type="password" name="passwordFirst">
            <input required maxlength="10" placeholder="  Повтори паролата" type="password" name="passwordSecond">
            <div id="errors">
            </div>
            <button id="submitButton" class="button " type="submit">Регистрация</button>
        </form>
      	<form action="../views/login.php"  method="GET">
            <div><button class="button " type="submit">Влез със съществуващ профил</button></div>
        </form>
    </div>
</div>
    <span id="demo">
    </span>
</body>
<script type="text/javascript" src="../js/register.js"></script>
<script type="text/javascript">
    const urlParams = new URLSearchParams(window.location.search);
    const myParam = urlParams.get('failed');
    if(myParam == "true"){
        const errors = document.createElement("div")
        errors.setAttribute("id", "erros")
        const node = document.createElement("p");
        node.innerHTML = "Това потребителско име вече съществува."
        errors.appendChild(node)
        const submitBtn = document.getElementById("submitButton")
        const form = document.getElementById('form')
        form.insertBefore(errors, submitBtn)
    }
</script>

</html>
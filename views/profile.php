<?php include "templates/init.php";?>
<!DOCTYPE html>
<html>
<?php include "templates/head.php";?>

   <link rel="stylesheet" type="text/css" href="../styles/homeStyle.css">
   <link rel="stylesheet" type="text/css" href="../styles/statsStyle.css">
   <link rel="stylesheet" type="text/css" href="../styles/tables.css">
   <link rel="stylesheet" type="text/css" href="../styles/profile.css">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <body>
      <?php include "templates/menu.php";?>
      <div id="content">
         <?php if (array_key_exists('nickname', $_SESSION)): ?>
         <div class="user-profile">
                    <div class="heading">
                        <h3 class="title">Лични данни</h3>
                    </div>
                    <div class="body">
                        <div>
                            <div>
                                <img src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=100" alt="User Pic">
                            </div>
                            <div>
                                <strong><?=$_SESSION["nickname"]?></strong><br>
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <td>Брой любими песни:</td>
                                        <td><?=count(User::getAllFavouriteSongs($conn, $_SESSION["nickname"]))?></td>
                                    </tr>
                                    <tr>
                                        <td>Най-слушана песен:</td>
                                        <?php if (Song::getMostListenSong($conn, $_SESSION["nickname"]) == false): ?>
                     <td> Няма данни</td>
               <?php else: ?>
                     <td><?=Song::getSongById($conn, Song::getMostListenSong($conn, $_SESSION["nickname"]))?></td>
               <?php endif?>
                                    </tr>
                                    <tr>
                                        <td>Общо време слушане на музика:</td>
                                        <td><?=Song::getAllListenTime($conn, $_SESSION["nickname"])?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
         <?php endif?>


      </div>
   </body>
   <script type="text/javascript" src="../js/common.js"></script>
</html>
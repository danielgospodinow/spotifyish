<nav class="parent">
<div id='brand'>
    <a >Spotifyish</a> 
</div>
<ul>
<?php if (array_key_exists('nickname', $_SESSION)): ?>
<li id='homeNav' onclick="markSelected(this)">
    <a href="./home.php">Начало</a>
</li>

<li id='profileNav' onclick="markSelected(this)">
<a href="./profile.php">
    Профил
</a>
</li>

<li id='statsNav' onclick="markSelected(this)">
<a href="./stats.php">
    Статистика
</a>
</li>

<li id='usersNav' onclick="markSelected(this)">
<a href="./allUsers.php">
    Потребители
</a>   
</li>

<li onclick="markSelected()">
<a href="./upload.php">
    Качи песен
</a>   
</li>
</ul>

<div id="loginPanel">
<p>Добре дошъл,  <?=$_SESSION["nickname"]?>  </p>
<?php else: ?>
<div id="loginPanel">
   <p>Добре дошъл,Гост </p>
   <?php endif?>
   <?php if (!array_key_exists('nickname', $_SESSION)): ?>
   <form action="../views/login.php" method="GET">
      <button class="button " type="submit"><i class="fa fa-sign-in"></i></button>
   </form>
   <span class="clear"></span>
</div>
<span class="clear"></span>
</nav>
<?php else: ?>
<form action="../controllers/logoutController.php" method="POST">
<button class="button " type="submit"><i class="fa fa-sign-out"></i></button>
</form>
<span class="clear"></span>
<?php endif?>
</div>
<span class="clear"></span>
</nav>